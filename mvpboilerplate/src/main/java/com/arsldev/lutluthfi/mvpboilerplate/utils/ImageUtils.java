package com.arsldev.lutluthfi.mvpboilerplate.utils;

import android.content.Context;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

public final class ImageUtils {

    public static void loadImage(Context context, String url, ImageView imageView) {
        Glide.with(context).load(url).asBitmap().centerCrop().into(imageView);
    }
}
